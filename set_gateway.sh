#!/bin/bash

# This script has to be executed under the superuser

# Downloading necessary packages
apt install iptables iptables-persistent

# Setting environment variables
export IF_NAME=$INNER_IF_NAME
export IPv4_ADDRESS=$INNER_IP_ADDRESS
export CIDR_BITMASK=$(./cn $INNER_NETMASK)
export INTERFACES_CONF=/etc/network/interfaces

export SYSCTL_CONF=/etc/sysctl.conf
export OUTPUT_IF_NAME=$OUTER_IF_NAME
export IPTABLES_RULES_FILE=/etc/iptables/rules.v4

# Setting up systemd-networkd
./ri interfaces.conf >> $INTERFACES_CONF
systemctl enable systemd-networkd
systemctl start systemd-networkd
ifup $IF_NAME

# Turning on ip forwarding
echo "net.ipv4.ip_forward=1" >> $SYSCTL_CONF
sysctl net.ipv4.ip_forward=1

# Setting up and saving iptables
iptables -t nat -A POSTROUTING -o $OUTPUT_IF_NAME -j MASQUERADE
iptables-save > $IPTABLES_RULES_FILE
