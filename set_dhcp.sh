#!/bin/bash

# This file has to be executed under the superuser

# Installing necessary packages
apt install isc-dhcp-server

# Setting environment variables
export DNS_SERVERS="$INNER_IP_ADDRESS, 8.8.8.8"
export SUBNET_ADDRESS="192.168.1.0"
export SUBNET_NETMASK=$INNER_NETMASK
export DHCP_ADDRESS_RANGE="192.168.1.101 192.168.1.199"
export DEFAULT_GATEWAY=$INNER_IP_ADDRESS

export DHCP_IF_NAME=$INNER_IF_NAME
export ISC_CONFIG_FILENAME=/etc/default/isc-dhcp-server
export DHCP_CONFIG_FILENAME=/etc/dhcp/dhcpd.conf

# Setting up dhcp-server
mv $ISC_CONFIG_FILENAME $ISC_CONFIG_FILENAME.default
echo "INTERFACES=\"$DHCP_IF_NAME\"" > $ISC_CONFIG_FILENAME

mv $DHCP_CONFIG_FILENAME $DHCP_CONFIG_FILENAME.default
./ri dhcpd.conf > $DHCP_CONFIG_FILENAME

# Restarting DHCP-server
systemctl restart isc-dhcp-server
