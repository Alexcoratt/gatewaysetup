#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

int power(int base, unsigned exp) {
	int res = 1;
	while (exp--)
		res *= base;
	return res;
}

char *convert_to_netmask(char cidr) {
	char *res = calloc(4, sizeof(char));
	for (char i = 0; i < 4; ++i) {
		for (char j = 0; j < 8 && cidr; ++j) {
			res[i] += power(2, 7 - j);
			--cidr;
		}
	}
	return res;
}

char convert_to_cidr(char const *netmask) {
	char res = 0;
	for (char i = 0; i < 4; ++i) {
		char num = netmask[i];
		while (num) {
			res += num & 1;
			num = (255 & num) >> 1;
		}
	}
	return res;
}

int chtoi(char value) {
	return 255 & value;
}

bool isCIDR(char const *line) {
	for (char const *s = line; *s; ++s)
		if (*s == '.')
			return false;
	return true;
}

bool is_valid_num(char num) {
	char last_bit = 0;
	while (num) {
		char current_bit = num & 1;
		if (1 ^ ((1 ^ last_bit) | current_bit))
		      return false;
		last_bit = current_bit;
		num = (255 & num) >> 1;
	}
	return true;
}

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <IPv4 CIDR bitmask (x) | IPv4 netmask (x.x.x.x)>\n", argv[0]);
		return 1;
	}

	if (isCIDR(argv[1])) {
		int cidr = atoi(argv[1]);
		if (cidr > 32) {
			fprintf(stderr, "Invalid CIDR mask: \"%d\"\n", cidr);
			return 1;
		}
	
		char *netmask = convert_to_netmask((char)cidr);

		for (char i = 0; i < 3; ++i)
			printf("%d.", chtoi(netmask[i]));
		printf("%d\n", chtoi(netmask[3]));

		free(netmask);
	} else {
		char netmask[4];
		char const *begin = argv[1];
		for (char i = 0; i < 3; ++i) {
			char const *end = strchr(begin, '.');
			if (!end) {
				fprintf(stderr, "Invalid netmask: \"%s\"\n", argv[1]);
				return 1;
			}

			size_t distance = end - begin;
			char * num_line = calloc(distance + 1, sizeof(char));
			memcpy(num_line, begin, distance);
			long int num = atoi(num_line);
			
			if (num > 255 || !is_valid_num((char)num) || (i != 0 && num > chtoi(netmask[i - 1]))) {
				fprintf(stderr, "Invalid netmask: \"%s\"\n", argv[1]);
                                return 1;
			}
			
			netmask[i] = (char)atol(num_line);
		
			free(num_line);
			begin = end + 1;
		}
	
		netmask[3] = (char)atol(begin);
		printf("%d\n", chtoi(convert_to_cidr(netmask)));
	}

	return 0;
}
