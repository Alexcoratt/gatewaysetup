#!/bin/bash

# This script has to be executed under the superuser

# Installing necessary packages
apt install gcc

# Setting main setup variables
export INNER_IP_ADDRESS="192.168.1.1"
export INNER_NETMASK="255.255.255.0"
export INNER_IF_NAME=enp0s8
export OUTER_IF_NAME=enp0s3

# Compilling special utilities
gcc read_interpret.c -o ri
gcc convert_netmask.c -o cn

# Executing setups
echo "Setting up gateway"
bash set_gateway.sh

echo "Setting up DHCP"
bash set_dhcp.sh

echo "Setting up DNS"
bash set_dns.sh

# Removing compilled utilities
rm ./ri ./cn
