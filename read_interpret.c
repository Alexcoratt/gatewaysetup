#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define DEFAULT_BUFFER_SIZE 17

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		return 1;
	}

	FILE *fptr = fopen(argv[1], "r");
	if (!fptr) {
		fprintf(stderr, "Error occured while opening file named \"%s\"\n", argv[1]);
		return 1;
	}

	char symb = 0;
	char prev_symb = 0;

	while ((symb = fgetc(fptr)) != EOF) {
		//printf("symb: %c\n", symb);
		
		if (symb == '$' && prev_symb != '\\') {
			size_t buffer_size = DEFAULT_BUFFER_SIZE;
			char *buffer = calloc(buffer_size, sizeof(char));
			
			size_t i = 0;
			while ((symb = fgetc(fptr)) != EOF && (isalpha(symb) || isdigit(symb) || symb == '_')) {
				if (i >= buffer_size) {
					char *tmp = calloc(buffer_size * 2, sizeof(char));
					memcpy(tmp, buffer, buffer_size);
					free(buffer);
					buffer = tmp;
					buffer_size *= 2;
				}
				buffer[i++] = symb;
			}
			printf("%s%c", getenv(buffer), symb);
			free(buffer);
		} else {
			printf("%c", symb);
		}

		prev_symb = symb;
	}

	fclose(fptr);

	return 0;
}
